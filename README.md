# Memory notes

Collection of utilities for recording notes to aid learning.

For now, this includes:
* aliases for inclusion in bash to open a file and add a timestamp automatically
* a hacky python 3 script to extract blocks in that file given a search term

The former was inspired by an article on recording what you did: https://theptrk.com/2018/07/11/did-txt-file/


