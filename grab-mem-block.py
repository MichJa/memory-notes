
import sys
import re

filename = sys.argv[1]
search = sys.argv[2]

delimiter = r"^# 20\d{2}\-\d{2}\-\d{2} \d{2}:\d{2}:\d{2}"
regex = r"(" + delimiter + r".*?)(?=" + delimiter + r"|\Z)"

with open(filename, 'r') as my_file:
    line = my_file.read()
    result = re.findall(regex, line, re.MULTILINE|re.DOTALL)

    for m in result:
        has = re.search(search, m, re.I)
        if has:
            print("---------------------")
            print(m)

