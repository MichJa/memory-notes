
# opens ~/Documents/mem.md in vim
alias emem="vim ~/Documents/mem.md"

# opens ~/Documents/mem.md in vim, adds date time of now to the first line,
# then adds a new line and jumps to second line
alias mem="vim \"+1r! date +'\%Y-\%m-\%d \%H:\%M:\%S'\" \"+normal I# \" \"+normal o\" \"+normal 2gg\" ~/Documents/mem.md"

